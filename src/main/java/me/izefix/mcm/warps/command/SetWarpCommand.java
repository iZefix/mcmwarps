/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.utils.Text;
import me.izefix.mcm.warps.Warp;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class SetWarpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            if ((p.isOp() || p.hasPermission("mcm.setwarp") || p.hasPermission("mcm.*"))) {
                if (args.length >= 1) {
                    if (args[0].equalsIgnoreCase("setd")) {
                        if (args.length >= 3) {
                            Warp w = new Warp(args[1]);
                            if (w.exists()) {
                                StringBuilder b = new StringBuilder(args[2]);
                                for (int i = 3; i < args.length; i++) {
                                    b.append(' ');
                                    b.append(args[i]);
                                }
                                w.setDescription(b.toString());
                                p.sendMessage(MCManager.getPrefix() + "§aThe description has been changed.");
                            } else {
                                p.sendMessage(MCManager.getPrefix() + "§cThis warp does not exist!");
                            }
                        }
                    } else if (args[0].equalsIgnoreCase("seticon")) {

                    } else if (args.length >= 1) {
                        String permission = "warp." + args[0];
                        if (args.length >= 2) {
                            permission = args[1];
                        }
                        Warp w = new Warp(args[0], p.getLocation(), permission, null, true, Material.ENDER_PEARL);
                        p.sendMessage(MCManager.getPrefix() + "§aThe warp " + w.getName() + " has been successfully saved and enabled.");
                    }
                }
            } else {
                p.sendMessage(MCManager.getPrefix() + new Text("command.no_permission", Text.getLanguage(p)));
            }
            return true;
        }
        return false;
    }

}
