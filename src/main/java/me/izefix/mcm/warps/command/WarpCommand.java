/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.utils.Text;
import me.izefix.mcm.warps.Warp;
import me.izefix.mcm.warps.gui.WarpMenu;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class WarpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            if (args.length == 1) {
                Warp w = new Warp(args[0]);
                if (w.exists() && w.hasPermission(p)) {
                    if (w.isEnabled()) {
                        w.warp(p);
                    } else {
                        p.sendMessage(MCManager.getPrefix() + "§cThis Warp is currently disabled.");
                    }
                } else {
                    p.sendMessage(MCManager.getPrefix() + "§cYou either have insufficient privileges to access this Warp or it does not exist.");
                }
            } else {
                new WarpMenu(p, 0, Text.getLanguage(p), null).open(p);
                return true;
            }
        } else {
            Warp w = new Warp(args[0]);
            if (w.exists()) {
                if (w.isEnabled()) {
                    cs.sendMessage("Teleporting entity to " + w.getName());
                    Location loc = w.getDestination();
                    if (loc != null) {
                        cs.sendMessage("x: " + loc.getX());
                        cs.sendMessage("y: " + loc.getY());
                        cs.sendMessage("z: " + loc.getZ());
                        cs.sendMessage("yaw: " + loc.getYaw());
                        cs.sendMessage("pitch: " + loc.getPitch());
                        cs.sendMessage("World: " + loc.getWorld().getName());
                        Bukkit.getWorlds().get(0).getEntities().get(0).teleport(loc);
                    } else {
                        cs.sendMessage("loc is null!");
                    }
                } else {
                    cs.sendMessage(MCManager.getPrefix() + "§cThis Warp is currently disabled.");
                }
            } else {
                cs.sendMessage("This warp does not exist!");
            }
        }
        return true;
    }

}
