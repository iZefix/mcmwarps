/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps;

import com.betafase.mcmanager.MCManager;
import java.util.ArrayList;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author dbrosch
 */
public class Warp {
    
    String name;
    Location loc = null;
    String permission = null;
    boolean enabled = true;
    String description = null;
    Material icon = Material.ENDER_PEARL;

    /**
     * Tries to retreive a warp from disc
     *
     * @param name the name of a warp
     */
    public Warp(String name) {
        this.name = name;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        if (cfg.contains(name)) {
            if (cfg.isString(name + ".description")) {
                description = cfg.getString(name + ".description");
            }
            if (cfg.isString(name + ".permission")) {
                permission = cfg.getString(name + ".permission");
            }
            if (cfg.isBoolean(name + ".enabled")) {
                enabled = cfg.getBoolean(name + ".enabled");
            }
            if (cfg.isString(name + ".icon")) {
                icon = Material.valueOf(cfg.getString(name + ".icon"));
            }
            World w = Bukkit.getWorld(cfg.getString(name + ".world"));
            if (w != null) {
                double x = cfg.getDouble(name + ".x");
                double y = cfg.getDouble(name + ".y");
                double z = cfg.getDouble(name + ".z");
                if (cfg.isDouble(name + ".yaw")) {
                    float yaw = (float) cfg.getDouble(name + ".yaw");
                    float pitch = (float) cfg.getDouble(name + ".pitch");
                    loc = new Location(w, x, y, z, yaw, pitch);
                } else {
                    loc = new Location(w, x, y, z);
                }
            } else {
                loc = null;
            }
        }
    }

    /**
     * Creates a new warp or overrides an existing one
     *
     * @param name
     * @param loc
     * @param permission
     * @param description
     * @param enabled
     */
    public Warp(String name, Location loc, String permission, String description, boolean enabled, Material icon) {
        this.name = name;
        this.loc = loc;
        this.permission = permission;
        this.enabled = enabled;
        this.description = description;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        cfg.set(name, null);
        if (description != null) {
            cfg.set(name + ".description", description);
        }
        cfg.set(name + ".permission", permission);
        cfg.set(name + ".enabled", enabled);
        cfg.set(name + ".icon", icon.name());
        
        cfg.set(name + ".world", loc.getWorld().getName());
        cfg.set(name + ".x", loc.getX());
        cfg.set(name + ".y", loc.getY());
        cfg.set(name + ".z", loc.getZ());
        cfg.set(name + ".yaw", (double) loc.getYaw());
        cfg.set(name + ".pitch", (double) loc.getPitch());
        
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public Warp(String name, Location loc) {
        this(name, loc, "warp." + name, null, true, Material.ENDER_PEARL);
    }
    
    public boolean exists() {
        return loc != null;
    }
    
    public String getName() {
        return name;
    }
    
    public Location getDestination() {
        return loc;
    }
    
    public String getPermission() {
        return permission;
    }
    
    public boolean canEdit(Permissible perm) {
        return (perm.isOp() || perm.hasPermission("editwarp." + name) || perm.hasPermission("editwarp.*") || perm.hasPermission("mcm.*") || perm.hasPermission("mcm.setwarp"));
    }
    
    public boolean hasPermission(Permissible perm) {
        return permission == null || perm.hasPermission(permission) || (perm.isOp() || perm.hasPermission("warp.*") || perm.hasPermission("mcm.*"));
    }
    
    public boolean isEnabled() {
        return enabled;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        
        cfg.set(name + ".description", description);
        
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public void setDestination(Location loc) {
        this.loc = loc;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        
        cfg.set(name + ".world", loc.getWorld().getName());
        cfg.set(name + ".x", loc.getX());
        cfg.set(name + ".y", loc.getY());
        cfg.set(name + ".z", loc.getZ());
        cfg.set(name + ".yaw", (double) loc.getYaw());
        cfg.set(name + ".pitch", (double) loc.getPitch());
        
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public Warp rename(String name) {
        delete();
        return new Warp(name, loc, permission, description, enabled, icon);
    }
    
    public void delete() {
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        cfg.set(name, null);
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        
        cfg.set(name + ".enabled", enabled);
        
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public void setPermission(String permission) {
        this.permission = permission;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        
        cfg.set(name + ".permission", permission);
        
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public void setIcon(Material icon) {
        this.icon = icon;
        FileConfiguration cfg = MCMWarpPlugin.getWarpConfiguration();
        
        cfg.set(name + ".icon", icon.name());
        
        MCMWarpPlugin.saveWarpConfiguration();
    }
    
    public Material getIcon() {
        return icon;
    }
    
    public void warp(Player p) {
        int delay = MCMWarpPlugin.getConfiguration().getInt("delay", 0);
        boolean no_move = MCMWarpPlugin.getConfiguration().getBoolean("no_move");
        boolean animation = MCMWarpPlugin.getConfiguration().getBoolean("animation");
        final Location ploc = p.getLocation().clone();
        if (delay > 0) {
            delay--;
            delay = delay * 20;
            ValueWrapper<Integer> v = new ValueWrapper(delay);
            final ArrayList<Location> display_loc = getCircle(ploc, 0.75, 30, animation);
            new BukkitRunnable() {
                
                @Override
                public void run() {
                    if (!no_move || isInTolerance(ploc, p.getLocation())) {
                        if (v.getValue() > 0) {
                            if (v.getValue() % 20 == 0) {
                                p.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("§eWarping you in " + (v.getValue() / 20) + "s"));
                                if (animation) {
                                    p.playNote(p.getLocation(), Instrument.PIANO, Note.natural(1, Note.Tone.G));
                                    for (Location loc : display_loc) {
                                        p.spawnParticle(Particle.PORTAL, loc, 1);
                                    }
                                }
                            }
                            v.setValue(v.getValue() - 1);
                        } else {
                            teleport(p);
                            p.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("§aYou were warped to " + getName()));
                            display_loc.clear();
                            this.cancel();
                        }
                    } else {
                        p.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("§cWarp cancelled. You moved."));
                        this.cancel();
                    }
                }
            }.runTaskTimer(MCMWarpPlugin.getInstance(), 20L, 1L);
        } else if (!no_move || isInTolerance(ploc, p.getLocation())) {
            p.sendMessage(MCManager.getPrefix() + "§aWarping you to " + getName());
            p.teleport(loc);
        } else {
            p.sendMessage(MCManager.getPrefix() + "§cWarp cancelled. Please stand still while warping.");
        }
    }
    
    public void teleport(Player p) {
        boolean animation = MCMWarpPlugin.getConfiguration().getBoolean("animation");
        if (animation) {
            p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 2, false, false));
            p.teleport(getDestination());
        } else {
            p.teleport(getDestination());
        }
    }
    
    private ArrayList<Location> getCircle(Location center, double radius, int amount, boolean animation) {
        ArrayList<Location> locations = new ArrayList<>(amount);
        if (animation) {
            World world = center.getWorld();
            double increment = (2 * Math.PI) / amount;
            for (int i = 0; i < amount; i++) {
                double angle = i * increment;
                double x = center.getX() + (radius * Math.cos(angle));
                double z = center.getZ() + (radius * Math.sin(angle));
                locations.add(new Location(world, x, center.getY(), z));
            }
        }
        return locations;
    }
    
    private boolean isInTolerance(Location a, Location b) {
        return a.toVector().distance(b.toVector()) < 0.75;
    }
    
}
