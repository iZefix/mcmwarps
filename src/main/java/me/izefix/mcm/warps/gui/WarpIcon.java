/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps.gui;

import com.betafase.mcmanager.api.ExecutableMenuItem;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.utils.Text;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author dbrosch
 */
public class WarpIcon implements ExecutableMenuItem {

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        new WarpMenu(p, 0, Text.getLanguage(p), null).open(p);
    }

    @Override
    public MenuItem getDisplayItem(String lang) {
        return new MenuItem(Material.EYE_OF_ENDER, "§9§lWarps");
    }

}
