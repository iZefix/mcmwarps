/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps.gui;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.MCManagerAPI;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.SafetyPrompt;
import com.betafase.mcmanager.api.SignInputHandler;
import java.util.LinkedList;
import me.izefix.mcm.warps.MCMWarpPlugin;
import me.izefix.mcm.warps.Warp;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.ChatPaginator;

/**
 *
 * @author dbrosch
 */
public class WarpManageMenu extends Menu {

    Warp w;

    public WarpManageMenu(Warp w, String lang) {
        super("§9Warp §7" + w.getName(), 36, lang);
        this.w = w;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        setItem(31, GUIUtils.back(lang));
        setWarpItem();

        setItem(GUIUtils.convertCounterSlot(1), new MenuItem(Material.NAME_TAG, "§eRename"));
        setEnabledItem();
        setItem(GUIUtils.convertCounterSlot(5), new MenuItem(Material.BOOK_AND_QUILL, "§7Change Description"));
        setItem(GUIUtils.convertCounterSlot(7), new MenuItem(Material.IRON_DOOR, "§7Change Permission"));
        setItem(GUIUtils.convertCounterSlot(10), new MenuItem(w.getIcon(), "§aDrag new Icon here"));
        this.setItem(GUIUtils.convertCounterSlot(13), new MenuItem(Material.LAVA_BUCKET, "§cDelete"));
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        setAutoCancel(true);
        int converted = GUIUtils.convertInventorySlot(e.getSlot());
        if (converted != -1) {
            switch (converted) {
                case 1:
                    if (!MCManagerAPI.requestSignTextInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] line) {
                            String input = line[0] + line[1] + line[2] + line[3];
                            if (!input.isEmpty()) {
                                Warp newWarp = w.rename(line[0]);
                                new WarpManageMenu(newWarp, lang).open(p);
                            } else {
                                WarpManageMenu.this.open(p);
                            }
                        }

                        @Override
                        public String[] getDefault() {
                            return null;
                        }
                    })) {
                        p.sendMessage(MCManager.getPrefix() + "§cProtocolLib is not installed!");
                    }
                    break;
                case 3:
                    w.setEnabled(!w.isEnabled());
                    setEnabledItem();
                    setWarpItem();
                    break;
                case 5:
                    e.getView().close();
                    TextComponent text = new TextComponent("§aClick §nhere§r§a to change the description");
                    text.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/setwarp setd " + w.getName() + " " + w.getDescription()));
                    p.spigot().sendMessage(new TextComponent(MCManager.getPrefix()), text);
                    break;
                case 7:
                    if (!MCManagerAPI.requestSignTextInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] line) {
                            String input = line[0] + line[1] + line[2] + line[3];
                            input = input.trim();
                            if (!input.isEmpty()) {
                                w.setPermission(input);
                                setWarpItem();
                            }
                            WarpManageMenu.this.open(p);
                        }

                        @Override
                        public String[] getDefault() {
                            return null;
                        }
                    })) {
                        p.sendMessage(MCManager.getPrefix() + "§cProtocolLib is not installed!");
                    }
                    break;
                case 10:
                    if (e.getAction() == InventoryAction.SWAP_WITH_CURSOR) {
                        setAutoCancel(false);
                        ItemStack cursor = e.getCursor();
                        Material mat = cursor.getType();
                        e.setCurrentItem(new ItemStack(Material.AIR));
                        e.setCursor(new ItemStack(Material.AIR));
                        w.setIcon(mat);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                MenuItem display = new MenuItem(mat, "§aDrag new Icon here");
                                e.setCurrentItem(display);
                                p.getInventory().addItem(cursor);
                            }
                        }.runTaskLater(MCMWarpPlugin.getInstance(), 5L);
                    }
                    break;
                case 13:
                    new SafetyPrompt("§7Are you sure?", lang) {
                        @Override
                        public void onApprove(boolean bln) {
                            if (bln) {
                                w.delete();
                                new WarpMenu(p, 0, lang, null).open(p);
                            } else {
                                WarpManageMenu.this.open(p);
                            }
                        }
                    }.open(p);
                    break;
            }
        } else {
            switch (e.getSlot()) {
                case 31:
                    new WarpMenu(p, 0, lang, null).open(p);
                    break;
            }
        }
    }

    public final void setWarpItem() {
        MenuItem warp = new MenuItem(w.getIcon());
        ItemMeta warpMeta = warp.getItemMeta();
        warpMeta.setDisplayName("§6§l" + w.getName());
        LinkedList<String> meta = new LinkedList<>();
        if (w.getDescription() != null) {
            for (String s : ChatPaginator.wordWrap(w.getDescription(), 40)) {
                meta.add("§7" + ChatColor.stripColor(s));
            }
            meta.add(" ");
        }
        meta.add("§8Permission: §6" + w.getPermission());
        meta.add("§8Enabled: §6" + w.isEnabled());
        Location dest = w.getDestination();
        meta.add("§8X: §6" + dest.getX());
        meta.add("§8Y: §6" + dest.getY());
        meta.add("§8Z: §6" + dest.getZ());
        warpMeta.setLore(meta);
        warp.setItemMeta(warpMeta);
        setItem(4, warp);
    }

    public final void setEnabledItem() {
        if (w.isEnabled()) {
            setItem(GUIUtils.convertCounterSlot(3), new MenuItem(Material.EMERALD_BLOCK, "§aEnabled"));
        } else {
            setItem(GUIUtils.convertCounterSlot(3), new MenuItem(Material.REDSTONE_BLOCK, "§cDisabled"));
        }
    }

}
