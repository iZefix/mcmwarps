/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps.gui;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.MCManagerAPI;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.SignInputHandler;
import com.betafase.mcmanager.gui.MainMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.util.ArrayList;
import java.util.LinkedList;
import me.izefix.mcm.warps.MCMWarpPlugin;
import me.izefix.mcm.warps.Warp;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.ChatPaginator;

/**
 *
 * @author dbrosch
 */
public class WarpMenu extends Menu {

    ArrayList<Warp> warps;
    int page;

    public WarpMenu(Player p, int page, String lang, ArrayList<Warp> warps) {
        super("§9Warps", 36, lang);
        this.page = page;
        this.warps = warps;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        if (ModuleManager.isValid(p, "use")) {
            this.setItem(31, GUIUtils.back(lang));
        }
        final boolean set_warp = ModuleManager.isValid(p, "setwarp");
        if (set_warp) {
            setItem(35, new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BLUE, "§9Settings"));
        }
        if (page > 0) {
            setItem(30, GUIUtils.previous_page(lang, page));
        }
        setItem(27, new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BROWN, "§eSearch by Name"));
        Bukkit.getScheduler().runTaskAsynchronously(MCMWarpPlugin.getInstance(), () -> {
            if (WarpMenu.this.warps == null) {
                WarpMenu.this.warps = MCMWarpPlugin.getWarps(p);
            }
            int counter = -page * 14;
            for (Warp w : WarpMenu.this.warps) {
                if (counter < 0) {
                    counter++;
                } else if (counter == 14) {
                    setItem(32, GUIUtils.next_page(lang, page + 2));
                    break;
                } else {
                    MenuItem warp = new MenuItem(w.getIcon());
                    ItemMeta warpMeta = warp.getItemMeta();
                    warpMeta.setDisplayName("§6§l" + w.getName());
                    LinkedList<String> meta = new LinkedList<>();
                    if (w.getDescription() != null) {
                        for (String s : ChatPaginator.wordWrap(w.getDescription(), 40)) {
                            meta.add("§7" + ChatColor.stripColor(s));
                        }
                        meta.add(" ");
                    }
                    if (set_warp) {
                        meta.add("§8Right Click to enter settings");

                    }
                    if (w.isEnabled()) {
                        meta.add("§a» Click to warp");
                    } else {
                        meta.add("§c» Warp is disabled");
                    }
                    warpMeta.setLore(meta);
                    warp.setItemMeta(warpMeta);
                    setItem(GUIUtils.convertCounterSlot(counter), warp);
                    counter++;
                }
            }
        });
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        switch (slot) {
            case 31:
                if (ModuleManager.isValid(p, "use")) {
                    new MainMenu(p, lang).open(p);
                }
                break;
            case 35:
                if (ModuleManager.isValid(p, "setwarp")) {
                    try {
                        MCManagerAPI.openPluginMenu(p, MCMWarpPlugin.getInstance());
                    } catch (Exception ex) {
                        p.sendMessage(MCManager.getPrefix() + new Text("command.no_permission", lang));
                    }
                }
                break;
            case 32:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new WarpMenu(p, page + 1, lang, warps).open(p);
                }
                break;
            case 30:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new WarpMenu(p, page - 1, lang, warps).open(p);
                }
                break;
            case 27:
                if (!MCManagerAPI.requestSignTextInput(p, new SignInputHandler() {
                    @Override
                    public void handleTextInput(String[] lines) {
                        ArrayList<Warp> result = new ArrayList<>();
                        if (!lines[0].isEmpty()) {
                            for (Warp w : warps) {
                                if (w.getName().toLowerCase().contains(lines[0].toLowerCase())) {
                                    result.add(w);
                                }
                            }
                        } else {
                            result = null;
                        }
                        new WarpMenu(p, 0, lang, result).open(p);
                    }

                    @Override
                    public String[] getDefault() {
                        return null;
                    }
                })) {
                    p.sendMessage(MCManager.getPrefix() + "§cProtocolLib is not installed!");
                }
                break;
            default:
                int converted = GUIUtils.convertInventorySlot(slot);
                if (converted != -1) {
                    int id = page * 14 + converted;
                    if (warps.size() > id) {
                        Warp w = warps.get(id);
                        if (e.getClick() == ClickType.RIGHT && w.canEdit(p)) {
                            new WarpManageMenu(w, lang).open(p);
                        } else if (w.isEnabled()) {
                            e.getView().close();
                            w.warp(p);
                        }
                    }
                }
                break;
        }
    }

}
