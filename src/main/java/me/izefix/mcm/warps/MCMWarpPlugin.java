/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.mcm.warps;

import com.betafase.mcmanager.api.MCManagerAPI;
import com.betafase.mcmanager.api.SpigotUpdateChecker;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import me.izefix.mcm.warps.command.SetWarpCommand;
import me.izefix.mcm.warps.command.WarpCommand;
import me.izefix.mcm.warps.gui.WarpIcon;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author dbrosch
 */
public class MCMWarpPlugin extends JavaPlugin {
    
    private static MCMWarpPlugin instance;
    private FileConfiguration warps;
    
    @Override
    public void onEnable() {
        instance = this;
        File warpFile = new File(getDataFolder(), "warps.yml");
        warps = YamlConfiguration.loadConfiguration(warpFile);
        saveWarpConfiguration();
        File f = new File(getDataFolder(), "config.yml");
        if (!f.exists()) {
            saveDefaultConfig();
        }
        getConfig().options().copyDefaults(true);
        MCManagerAPI.addMenuItem(new WarpIcon());
        Bukkit.getPluginCommand("setwarp").setExecutor(new SetWarpCommand());
        Bukkit.getPluginCommand("warp").setExecutor(new WarpCommand());
        MCManagerAPI.getPluginHook(this).setUpdateChecker(new SpigotUpdateChecker(this, 39425));
    }
    
    public static FileConfiguration getWarpConfiguration() {
        return instance.warps;
    }
    
    public static ArrayList<Warp> getWarps(Player p) {
        ArrayList<Warp> col = new ArrayList<>();
        for (String key : getWarpConfiguration().getKeys(false)) {
            Warp w = new Warp(key);
            if (w.exists() && w.hasPermission(p)) {
                col.add(w);
            }
        }
        return col;
    }
    
    public static void saveWarpConfiguration() {
        instance._saveWarps();
    }
    
    public static MCMWarpPlugin getInstance() {
        return instance;
    }
    
    public static void saveConfiguration() {
        instance.saveConfig();
    }
    
    public static FileConfiguration getConfiguration() {
        return instance.getConfig();
    }

    /**
     * Internal method
     */
    public void _saveWarps() {
        try {
            warps.save(new File(getDataFolder(), "warps.yml"));
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "An error occured while saving the warps", ex);
        }
    }
    
}
